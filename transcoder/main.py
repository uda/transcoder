#!/usr/bin/env python3

import tkinter as tk
from pathlib import Path
from tkinter.filedialog import askopenfilenames

DEFAULT_SOURCE_ENCODING = 'ascii'
DEFAULT_TARGET_ENCODING = 'utf_8'

# Based on 3.10 supported encodings from https://docs.python.org/3.10/library/codecs.html#standard-encodings
SUPPORTED_ENCODINGS = (
    'ascii', 'big5', 'big5hkscs', 'cp037', 'cp273', 'cp424', 'cp437', 'cp500', 'cp720', 'cp737', 'cp775', 'cp850',
    'cp852', 'cp855', 'cp856', 'cp857', 'cp858', 'cp860', 'cp861', 'cp862', 'cp863', 'cp864', 'cp865', 'cp866', 'cp869',
    'cp874', 'cp875', 'cp932', 'cp949', 'cp950', 'cp1006', 'cp1026', 'cp1125', 'cp1140', 'cp1250', 'cp1251', 'cp1252',
    'cp1253', 'cp1254', 'cp1255', 'cp1256', 'cp1257', 'cp1258', 'euc_jp', 'euc_jis_2004', 'euc_jisx0213', 'euc_kr',
    'gb2312', 'gbk', 'gb18030', 'hz', 'iso2022_jp', 'iso2022_jp_1', 'iso2022_jp_2', 'iso2022_jp_2004', 'iso2022_jp_3',
    'iso2022_jp_ext', 'iso2022_kr', 'latin_1', 'iso8859_2', 'iso8859_3', 'iso8859_4', 'iso8859_5', 'iso8859_6',
    'iso8859_7', 'iso8859_8', 'iso8859_9', 'iso8859_10', 'iso8859_11', 'iso8859_13', 'iso8859_14', 'iso8859_15',
    'iso8859_16', 'johab', 'koi8_r', 'koi8_t', 'koi8_u', 'kz1048', 'mac_cyrillic', 'mac_greek', 'mac_iceland',
    'mac_latin2', 'mac_roman', 'mac_turkish', 'ptcp154', 'shift_jis', 'shift_jis_2004', 'shift_jisx0213', 'utf_32',
    'utf_32_be', 'utf_32_le', 'utf_16', 'utf_16_be', 'utf_16_le', 'utf_7', 'utf_8', 'utf_8_sig',
)

# This is not yet used, but should be used in the future to find the relevant encoding
SUPPORTED_ENCODING_ALIASES = (
    '646', 'us-ascii', 'big5-tw', 'csbig5', 'big5-hkscs', 'hkscs', 'IBM037', 'IBM039', '273', 'IBM273', 'csIBM273',
    'EBCDIC-CP-HE', 'IBM424', '437', 'IBM437', 'EBCDIC-CP-BE', 'EBCDIC-CP-CH', 'IBM500', 'IBM775', '850', 'IBM850',
    '852', 'IBM852', '855', 'IBM855', '857', 'IBM857', '858', 'IBM858', '860', 'IBM860', '861', 'CP-IS', 'IBM861',
    '862', 'IBM862', '863', 'IBM863', 'IBM864', '865', 'IBM865', '866', 'IBM866', '869', 'CP-GR', 'IBM869', '932',
    'ms932', 'mskanji', 'ms-kanji', '949', 'ms949', 'uhc', '950', 'ms950', 'ibm1026', '1125', 'ibm1125', 'cp866u',
    'ruscii', 'ibm1140', 'windows-1250', 'windows-1251', 'windows-1252', 'windows-1253', 'windows-1254', 'windows-1255',
    'windows-1256', 'windows-1257', 'windows-1258', 'eucjp', 'ujis', 'u-jis', 'jisx0213', 'eucjis2004', 'eucjisx0213',
    'euckr', 'korean', 'ksc5601', 'ks_c-5601', 'ks_c-5601-1987', 'ksx1001', 'ks_x-1001', 'chinese', 'csiso58gb231280',
    'euc-cn', 'euccn', 'eucgb2312-cn', 'gb2312-1980', 'gb2312-80', 'iso-ir-58', '936', 'cp936', 'ms936', 'gb18030-2000',
    'hzgb', 'hz-gb', 'hz-gb-2312', 'csiso2022jp', 'iso2022jp', 'iso-2022-jp', 'iso2022jp-1', 'iso-2022-jp-1',
    'iso2022jp-2', 'iso-2022-jp-2', 'iso2022jp-2004', 'iso-2022-jp-2004', 'iso2022jp-3', 'iso-2022-jp-3',
    'iso2022jp-ext', 'iso-2022-jp-ext', 'csiso2022kr', 'iso2022kr', 'iso-2022-kr', 'iso-8859-1', 'iso8859-1', '8859',
    'cp819', 'latin', 'latin1', 'L1', 'iso-8859-2', 'latin2', 'L2', 'iso-8859-3', 'latin3', 'L3', 'iso-8859-4',
    'latin4', 'L4', 'iso-8859-5', 'cyrillic', 'iso-8859-6', 'arabic', 'iso-8859-7', 'greek', 'greek8', 'iso-8859-8',
    'hebrew', 'iso-8859-9', 'latin5', 'L5', 'iso-8859-10', 'latin6', 'L6', 'iso-8859-11', 'thai', 'iso-8859-13',
    'latin7', 'L7', 'iso-8859-14', 'latin8', 'L8', 'iso-8859-15', 'latin9', 'L9', 'iso-8859-16', 'latin10', 'L10',
    'cp1361', 'ms1361', 'kz_1048', 'strk1048_2002', 'rk1048', 'maccyrillic', 'macgreek', 'maciceland', 'maclatin2',
    'maccentraleurope', 'mac_centeuro', 'macroman', 'macintosh', 'macturkish', 'csptcp154', 'pt154', 'cp154',
    'cyrillic-asian', 'csshiftjis', 'shiftjis', 'sjis', 's_jis', 'shiftjis2004', 'sjis_2004', 'sjis2004',
    'shiftjisx0213', 'sjisx0213', 's_jisx0213', 'U32', 'utf32', 'UTF-32BE', 'UTF-32LE', 'U16', 'utf16', 'UTF-16BE',
    'UTF-16LE', 'U7', 'unicode-1-1-utf-7', 'U8', 'UTF', 'utf8', 'cp65001',
)


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_frames()
        self.create_widgets()

        # Config
        self.initial_dir = Path.home()

    def create_frames(self):
        self.top_frame = tk.Frame(self, padx=10, pady=10)
        self.top_frame.pack()
        self.middle_frame = tk.Frame(self, padx=10, pady=10)
        self.middle_frame.pack()
        self.bottom_frame = tk.Frame(self, padx=10, pady=10)
        self.bottom_frame.pack()

    def create_widgets(self):
        self.file_selector = tk.Button(self.top_frame, text='Select files', command=self.select_files)
        self.file_selector.pack()

        self.transcode = tk.Button(self.bottom_frame, text="Transcode", command=self.transcode)
        self.transcode.pack()
        self.close = tk.Button(self.bottom_frame, text="Close", command=self.master.destroy)
        self.close.pack()

    def select_files(self):
        for child in self.middle_frame.winfo_children():
            child.destroy()
        file_list = askopenfilenames(title='Select files', initialdir=self.initial_dir)
        if len(file_list):
            self.initial_dir = Path(file_list[0]).parent
        self.files_var = tk.StringVar(value=file_list)
        self.selected_files = tk.Listbox(self.middle_frame, listvariable=self.files_var, height=10,
                                         selectmode='extended', width=50)
        self.selected_files.pack()
        self.selected_files.selection_set(first=0, last=tk.END)

        encoding_var = SUPPORTED_ENCODINGS

        self.source_encoding_value = tk.StringVar(value=DEFAULT_SOURCE_ENCODING)
        self.source_encoding = tk.OptionMenu(self.middle_frame, self.source_encoding_value, *encoding_var)
        self.source_encoding.pack(side='left')

        self.target_encoding_value = tk.StringVar(value=DEFAULT_TARGET_ENCODING)
        self.target_encoding = tk.OptionMenu(self.middle_frame, self.target_encoding_value, *encoding_var)
        self.target_encoding.pack(side='right')

        self.keep_original_value = tk.IntVar(value=1)
        self.keep_original = tk.Checkbutton(self.middle_frame, text='Keep original', variable=self.keep_original_value,
                                            onvalue=1, offvalue=0)
        self.keep_original.pack()

    def transcode(self):
        files_to_transcode = [self.selected_files.get(i) for i in self.selected_files.curselection()]

        for file_path in files_to_transcode:
            file_obj = Path(file_path)
            # Set last file's directory as the initial dir
            target_file = self.get_target_filename(file_obj) if self.keep_original_value.get() else file_obj
            target_file.write_text(
                data=file_obj.read_text(encoding=self.source_encoding_value.get()),
                encoding=self.target_encoding_value.get(),
            )

    def get_target_filename(self, file_obj: Path):
        filename = file_obj.name
        parts = filename.rsplit('.', 1)
        return file_obj.parent / '.'.join(parts[0:1] + [self.target_encoding_value.get()] + parts[1:])


def main():
    root = tk.Tk()
    application = Application(master=root)
    application.mainloop()


if __name__ == '__main__':
    main()
