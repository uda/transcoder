# Transcoder

Transcode multiple files between codecs

## Usage

- Execute the python file in the transcoder module
- Select the source files (each time you click on "Select files" the file list resets)
- Deselect files you don't want to transcode
- Select source and target encodings
- Click "Transcode", the transcoded files will be in the original directory

## Issues

- There is no safeguard for rerunning the transcoding, previously transcoded files will be overwritten
- The encoding list is huge and no reasonable way to search for the relevant encoding, I created a workaround by having initial values in the beginning of the script file 
